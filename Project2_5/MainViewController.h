//
//  MainViewController.h
//  Project2_5
//
//  Created by Ansel Duff on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlipsideViewController.h"
#import "HangmanModel.h"
#import "GoodHangman.h"
#import "EvilHangman.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate, UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) NSMutableArray *wordsArray;
@property (strong, nonatomic) NSString *chosenWord;
@property (nonatomic) int guessesCounter;
@property (weak, nonatomic) IBOutlet UILabel *guessesLabel;
@property (nonatomic) int letterSpawnX;
@property (strong, nonatomic) NSMutableArray *lettersArray;
@property (strong, nonatomic) NSMutableArray *outputText;

- (IBAction)showInfo:(id)sender;
- (IBAction)newGame:(id)sender;
- (IBAction)updateText:(id)sender;
- (int)check:(char)guess;
- (void)checkForLetter;
- (void)letterLabelCreate:(UILabel *)letterLabel withIndex: (int)i;
- (void) gameOver;
@end

