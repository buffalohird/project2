//
//  MainViewController.m
//  Project2_5
//
//  Created by Ansel Duff on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"
#import <string.h>
#import <stdlib.h>

#define LETTERSPAWNY1  180
#define LETTERSPAWNY2  210

@implementation MainViewController

@synthesize answerLabel = _answerLabel;
@synthesize textField = _textField;
@synthesize wordsArray = _wordsArray;
@synthesize chosenWord = _chosenWord;
@synthesize guessesCounter = _guessesCounter;
@synthesize guessesLabel = _guessesLabel;
@synthesize letterSpawnX = _letterSpawnX;
@synthesize lettersArray = _lettersArray;
@synthesize outputText = _outputText;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.textField.hidden = YES;
    [self.textField becomeFirstResponder];
    
    // set default values
    NSMutableDictionary *defaultValues = [[NSMutableDictionary alloc] init];
    [defaultValues setObject:@"3" forKey:@"length"];
    [defaultValues setObject:@"5" forKey:@"guesses"];
    [defaultValues setObject:@"0" forKey:@"hangmanMode"];
    
    // register default
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults registerDefaults:defaultValues];
    
    // initialize the lettersArray and wordArray
    self.lettersArray = [[NSMutableArray alloc] initWithObjects: nil];
    self.wordsArray = [[NSMutableArray alloc] initWithObjects: nil];
    
    /*
     * If you want to see the contents of the wordsArray
     */
    /*for (id obj in self.wordsArray)
     {
     NSLog(@" %@", obj);
     }*/
    
    // start a new game when the view loaded
    [self newGame:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)showInfo:(id)sender
{    
    FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideViewController" bundle:nil];
    controller.delegate = self;
    controller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentModalViewController:controller animated:YES];
}

- (IBAction)newGame:(id)sender {
    
    //removes all words to begin new game
    [self.wordsArray removeAllObjects];
    
    //remove all guessed letters from the screen to begin anew
    for (UILabel *s in self.lettersArray) {
        
        [s removeFromSuperview];
        
    }
    
    self.letterSpawnX = 0;
    
    //creates the guessable letters list
    for(int i = 0; i < 26; i++) {
        if( i < 13) {
            UILabel *letterLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.letterSpawnX, LETTERSPAWNY1, 20, 15)];
            
            [self letterLabelCreate:letterLabel withIndex:i];
        }
        else {
            if( i == 13)
                self.letterSpawnX = 0;
            
            UILabel *letterLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.letterSpawnX, LETTERSPAWNY2, 20, 15)];
            
            [self letterLabelCreate:letterLabel withIndex:i];
        }
        
    }
    
    self.guessesLabel.text = @"Guess A Letter!";
    
    // load the dictionary from our plist
    NSDictionary *words = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"staff" ofType:@"plist"]];
    
    // again, load our default
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // iterate through the words, and match any of the length specified by the user
    for (NSString *tf in [words valueForKey:@"staff"]) {
        
        // match
        if([tf length] ==  [[defaults objectForKey:@"length"] intValue])
        {
            // add to our wordsArray
            [self.wordsArray addObject:tf];
        }
    }
    
    //corner case for if no words of suitable length exist (shouldn't happen in final version)
    if([self.wordsArray count] != 0) {
        
        // randomely pick a word, 'store' it
        int random = (arc4random() % [self.wordsArray count]);
        self.chosenWord = [self.wordsArray objectAtIndex:random];
        
        //if a word is chosen, we produce the correct number of blanks
        self.outputText = [[NSMutableArray alloc] init];
        
        // set up the array which the user sees (the hyphens)
        for(int counter = 0; counter < ([[defaults objectForKey:@"length"] intValue]); counter++)
        {
            [self.outputText addObject:@"- "];
        }
        
        // make the array into a string
        self.answerLabel.text = [self.outputText componentsJoinedByString:@""];
    }
    else 
    {
        NSLog(@"No Words");
    }
    
    // load the guesses counter from the user's chosen value
    self.guessesCounter = [[defaults stringForKey:@"guesses"] intValue];
    
}

- (void)letterLabelCreate:(UILabel *)letterLabel withIndex: (int)i {
    
    letterLabel.text = [NSString stringWithFormat: @"%c", (int)('A' + i )];
    letterLabel.textColor = [UIColor whiteColor];
    letterLabel.textAlignment   = UITextAlignmentCenter;
    letterLabel.tag = i;
    letterLabel.font = [UIFont systemFontOfSize:14];
    letterLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:letterLabel];
    self.letterSpawnX += 25;
    [self.lettersArray insertObject:letterLabel atIndex:i];
}

- (IBAction)updateText:(id)sender {
    
    //if it is the first guess of the game, we remove the instructions
    if(self.guessesLabel.text = @"Guess A Letter!")
        self.guessesLabel.text = [NSString stringWithFormat:@"Guesses Left: %d", self.guessesCounter];
    
    //if there is already a letter, we only select the latest letter entered
    if([self.textField.text length] > 1) {
        
        NSString *newOutput = [self.textField.text substringFromIndex:[self.textField.text length] -1];
        
        //we display the new character in string form
        self.textField.text = newOutput;
        
        //we create the char version to be passed to the check function
        char guess = [newOutput characterAtIndex:0];
        int answer = [self check:(guess)];
        
        //checks if letter is an available letter guess
        [self checkForLetter];
        
        // match, good guess. update the string
        if(answer != 0)
        {
            [self.outputText replaceObjectAtIndex:(answer -1) withObject:[NSString stringWithFormat: @"%c", guess]];
            self.answerLabel.text = [self.outputText componentsJoinedByString:@""];
            
            // alert the user that they've won
            if([[self.chosenWord uppercaseString] isEqualToString:[self.answerLabel.text uppercaseString]])
                [self gameOver];
        }
        // they guesses wrong, just display the old string
        else
        {
            self.answerLabel.text = [self.outputText componentsJoinedByString:@""];
        }
        
        //we clear the text field
        self.textField.text = [NSString stringWithFormat:(@"_")];
    }
    
    //if textField is blank, we need not delete previous data, but show it as a blank field (this happens if the user manually deletes)
    else if([self.textField.text length] == 0){
        NSString *newOutput = [NSString stringWithFormat:(@"_") ];
        //both fields are set to "_".  There is no letter to check
        self.textField.text = newOutput;
    }
    
}

-(void)checkForLetter {
    
    //this block of code checks if the entered letter is on the screen
    for (UILabel *s in self.lettersArray) {
        
        //we use index[0] to avoid issues with exact string values, only the first char exists for our purposes
        if([self.textField.text characterAtIndex:0] == [s.text characterAtIndex:0]) {
            s.text = @"_";
        }
        
    }
    
}

- (int)check:(char)guess
{
    // turn the randomely chosen word into a c string (array of chars)
    const char * cString = [self.chosenWord UTF8String];
    
    // iterate thru, look for matches against the user's guess
    for(int i = 0; i < strlen(cString); i++)
    {
        if((guess == toupper(cString[i])) || (guess == tolower(cString[i])))
        {
            return (i+1);
        }
    }
    //update the guess counter if it was an incorrect guess used for the first time
    for(UILabel *s in self.lettersArray) {
        
        if(guess == [s.text characterAtIndex:0]) {
            self.guessesCounter--;
            self.guessesLabel.text = [NSString stringWithFormat:@"Guesses Left: %d", self.guessesCounter];
            
            //send the gameOver message if there are no remaining guesses
            if(self.guessesCounter == 0) {
                [self gameOver];
            }
        }
    }
    
    return 0;
    
}

-(void)gameOver {
    
    //our case for losers
    if(self.guessesCounter == 0) {
        //displays an alert popup to the loser
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Game Over"
                              message:[NSString stringWithFormat:@"Your word was %@", self.chosenWord]
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"New game", @"Options", nil];
        [alert show];
    }
    
    //our case for winners
    else {
        //displays an alert popup to the winner
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"You Won!"
                              message:[NSString stringWithFormat:@"You guessed %@ correctly!", self.chosenWord]
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"New game", @"Options", nil];
        [alert show];
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    //when the user accepts the dialog we begin a new game
    if(buttonIndex == 0) {
        
        [self newGame: nil];
    }
    
    //or they may go straight to options to choose different settings
    if(buttonIndex == 1) {
        
        [self newGame: nil];
        [self showInfo:nil];
    }
    
}


@end
