
Ansel Duff
Buffalo Hird
Project2
Computer Science 164

README.txt - Instructions on constructing our iOSapplication.

What We've Done:

    * Completed all core functionality of hangman (nonevil)
    * Nearly completed our interface (History will see some changes)
    * Finished mechanisms we will use to load the larget plist and contain Evil hangman methods
    * Implemented user settings
    * Created methods to take user from a new game all the way to a game over alert
    * Hangman prototype created
    
What Remains To Be Done:

    * Evil Hangman
    * History
    * Final plist (right now we've used a smaller plist to make it a bit lighter/easier to debug)
    * Accept only proper characters (no random ASCII ones)
    * Factored code into separate model files - (evil and good hangman objects, protocols, etc.) rename these files to exactly match the spec. The location of our functional code right now is TEMPORARY.

Known Issues (every project has 'em):
  * We don't use the full plist and assume users won't hit other ASCII characters
  * First text input in a new game begets no response.  All further input works as expected (this is likely a condition bug, we aren't really worrying about it because we will be changing / cleaning up our conditions anyway)
  * There's a bug for words that have double letters. This will certainly be sorted out by the release.
  * The guessesCounter sometimes jumps. This will also be sorted out on the release.